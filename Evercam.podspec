Pod::Spec.new do |s|
  s.name             = "Evercam"
  s.version          = "1.1.5"
  s.summary          = "Objective C wrapper around Evercam API"
  s.homepage         = "http://www.evercam.io/"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Evercam" => "nain@evercam.io" }
  s.source           = { :git => "https://github.com/evercam/evercam-objc.git", :tag => '1.1.5' }

  s.platform         = :ios, '7.0'
  s.source_files     = 'Evercam/*.{h,m}'
  s.requires_arc     = true
  s.dependency      'AFNetworking', '~> 3.0'
end
